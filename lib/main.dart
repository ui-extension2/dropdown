import 'package:flutter/material.dart';
import 'package:ui_extension/checkbox_widget.dart';
import 'package:ui_extension/checkboxtile_widget.dart';
import 'package:ui_extension/dropdown_widget.dart';

void main() {
  runApp(MaterialApp(
      title: 'Ui Extension', home: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Ui Extension')),
        body: ListView(children: [
          ListTile(
            title: Text('CheckBox'),
            onTap: () {
              Navigator.push(
                context, 
                MaterialPageRoute(builder: (context) => CheckBoxWidget()));
            },
          ),
          ListTile(
            title: Text('CheckBoxTile'),
            onTap: () {
              Navigator.push(
                context, 
                MaterialPageRoute(builder: (context) => CheckBoxTileWidget()));
            },
          ),
           ListTile(
            title: Text('DropDownTile'),
            onTap: () {
              Navigator.push(
                context, 
                MaterialPageRoute(builder: (context) => DropDownWidget()));
            },
          ),
        ],),
      );
  }
}